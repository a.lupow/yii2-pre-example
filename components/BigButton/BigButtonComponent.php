<?php

namespace app\components\BigButton;

use PRE\Component;
use yii\pre\YiiComponent;

class BigButtonComponent extends YiiComponent {

    public $template = 'template.pre';
    protected $dependencies = [];

    public $success = false;
    public $href;

    /**
     * {@inheritDoc}
     */
    public function render($variables = [])
    {
        return parent::render($variables + [
                'success' => $this->success,
                'href' => $this->href,
            ]
        );
    }

}
