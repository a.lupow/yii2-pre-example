<?php

namespace app\components\Content;

use yii\pre\YiiComponent;

class ContentComponent extends YiiComponent {

    public $template = 'content.pre';

}
