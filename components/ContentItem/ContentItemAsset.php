<?php

namespace app\Components\ContentItem;

use yii\web\AssetBundle;

class ContentItemAsset extends AssetBundle {

    public $sourcePath = __DIR__;

    public $js = [
        'test.js'
    ];

}
