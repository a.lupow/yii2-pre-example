<?php

namespace app\components\ContentItem;

use yii\pre\YiiComponent;

class ContentItemComponent extends YiiComponent {

    public $template = 'contentItem.pre';

    protected $dependencies = [
        ContentItemAsset::class
    ];

    public $heading;

    public function render($variables = [])
    {
        return parent::render($variables + [
                'heading' => $this->heading,
            ]
        );
    }

}
