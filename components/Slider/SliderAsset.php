<?php

namespace app\components\Slider;

use yii\web\AssetBundle;

class SliderAsset extends AssetBundle {

    public $sourcePath = __DIR__ . '/assets';

    public $js = [
        './slick/slick.min.js',
        './js/slick.js',
    ];

    public $css = [
        './slick/slick.css',
        './slick/slick-theme.css',
    ];

}
