<?php

namespace app\components\Slider;

use PRE\AttributesBundle;
use yii\pre\YiiComponent;

class SliderComponent extends YiiComponent {

    public $template = 'template.pre';
    protected $dependencies = [
        SliderAsset::class,
    ];

    public $slickOptions = [];

    public function render($variables = [])
    {
        return parent::render($variables + [
                'attributes' => new AttributesBundle([
                    'class' => [],
                    'data-slick' => $this->slickOptions,
                ]),
            ]
        );
    }

}
