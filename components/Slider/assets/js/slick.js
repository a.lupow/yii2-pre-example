(function ($) {
  'use strict';

  $(document).ready(function () {
    $('.slick-slider').each(function () {
      $(this).slick();
    });
  });

})(jQuery);